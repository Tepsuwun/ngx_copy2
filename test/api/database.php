<?php
session_start();

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
header("Access-Control-Allow-Headers: *");
// $user = $_SESSION["user"];
$user = "admin";

if ($user == "admin"){
    echo '{
        "message": "เข้าสู่ระบบ Admin สำเร็จ ('.$user.')",
        "success": true
    }';
} else {
    echo '{
        "message": "เข้าสู่ระบบ Admin ไม่สำเร็จ",
        "success": false
    }';
}

?>
import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
    {
        title: 'admin',
        icon: 'ion-arrow-down-b',
        children: [
            {
                title: 'รายชื่อพนักงาน',
                link: '/admin/admin/alluser',
            },
            {
                title: 'Report',
                link: '/admin/admin/submenu2',
            },
        ],
    },
    {
        title: 'logOut',
        icon: 'ion-log-out',
        link: '/logout',
        home: true,
    },
];

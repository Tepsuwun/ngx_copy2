import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { Menu1RoutingModule, routedComponents } from './menu1-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    Menu1RoutingModule,
      Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class Menu1Module { }

import {Component, OnInit} from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { AuthService } from '../../../auth.service';

import { SmartTableService } from '../../../@core/data/smart-table.service';

@Component({
  selector: 'ngx-submenu1',
  styleUrls: ['./alluser.component.scss'],
  templateUrl: './alluser.component.html',
})
export class AlluserComponent implements OnInit {
    alluser = [];
    allmanager = [];
    ngOnInit() {
        const data = this.alluser;
        this.source.load(this.alluser);
        this.Auth.alluser().subscribe(search => {
            this.alluser = search.user;
            const newdata = this.alluser;
            this.source.load(this.alluser);
        })
        this.Auth.search_manager().subscribe(search => {
            this.allmanager = search.data;
            this.source2.load(this.allmanager);
        })
    }
    settings = {
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmCreate: true,
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmSave: true,
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        columns: {
            emp_id: {
                title: 'No.',
                type: 'number',
            },
            user_id: {
                title: 'Username',
                type: 'string',
            },
            user_pass: {
                title: 'Password',
                type: 'string',
            },
            user_status: {
                title: 'status',
                type: 'string',
            },
            emp_name: {
                title: 'name',
                type: 'string',
            },
            emp_nickname: {
                title: 'nickname',
                type: 'string',
            },
            emp_still: {
                title: 'สถานะการทำงาน',
                type: 'string',
            },

        },
    };

    settings2 = {
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmCreate: true,
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmSave: true,
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        columns: {
            emp_id: {
                title: 'No.',
                type: 'number',
            },
            user_id: {
                title: 'Username',
                type: 'string',
            },
            user_pass: {
                title: 'Password',
                type: 'string',
            },
        },
    };
    source: LocalDataSource = new LocalDataSource();
    source2: LocalDataSource = new LocalDataSource();
    constructor(private service: SmartTableService,
                private Auth: AuthService ) {
    }

    onDeleteConfirm(event): void {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
            console.log (event.data.emp_id);
            this.Auth.DeleteUser(event.data.emp_id).subscribe(data => {});
            this.loadData();
        } else {
            event.confirm.reject();
        }
    }
    onEditConfirm(event) {
        if (window.confirm('Are you sure you want to save?')) {
            event.confirm.resolve(event.newData);
            console.log (event.newData);
            const emp_id = event.newData.emp_id;
            const user_id = event.newData.user_id;
            const user_pass = event.newData.user_pass;
            const user_status = event.newData.user_status;
            switch (user_status) {
                case '1': {
                    this.user_status = '1';
                    break;
                }
                case '2': {
                    this.user_status = '2';
                    break;
                }
                case 'Employee': {
                    this.user_status = '1';
                    break;
                }
                case 'Supervisor': {
                    this.user_status = '2';
                    break;
                }
                default: {
                    this.user_status = '0';
                    break;
                }
            }
            if (this.user_status != '0') {
                console.log (status);
                this.Auth.EditUser(emp_id, user_id, user_pass , this.user_status).subscribe(data => {
                    // this.source.load(search.user);
                    // console.log (search.data);
                    // this.source.load(search.data);
                })
            }else {
            }
        } else {
            event.confirm.reject();
        }
    }

    onEditConfirm2(event) {
        if (window.confirm('Are you sure you want to save?')) {
            event.confirm.resolve(event.newData);
            console.log (event.newData);
            const emp_id = event.newData.emp_id;
            const user_id = event.newData.user_id;
            const user_pass = event.newData.user_pass;

            if (this.user_status != '0') {
                console.log (status);
                this.Auth.EditUser(emp_id, user_id, user_pass , 3).subscribe(data => {
                })
                this.loadData2();
            }else {
            }
        } else {
            event.confirm.reject();
        }
    }
    user_status: string;
    onCreateConfirm(event) {
        if (window.confirm('Are you sure you want to Create?')) {
            // console.log (event.newData.emp_id);
            const emp_id = event.newData.emp_id;
            const user_id = event.newData.user_id;
            const user_pass = event.newData.user_pass;
            const user_status = event.newData.user_status;
            switch (user_status) {
                case '1': {
                    this.user_status = '1';
                    break;
                }
                case '2': {
                    this.user_status = '2';
                    break;
                }
                case 'Employee': {
                    this.user_status = '1';
                    break;
                }
                case 'Supervisor': {
                    this.user_status = '2';
                    break;
                }
                default: {
                    this.user_status = '0';
                    break;
                }
            }
            if (this.user_status != '0') {
                console.log (status);
                this.Auth.AddUser(emp_id, user_id, user_pass , this.user_status).subscribe(data => {
                    // this.source.load(search.user);
                    // console.log (search.data);
                    // this.source.load(search.data);
                })
            }else {
            }
            event.confirm.resolve();
            this.loadData();
        } else {
            event.confirm.reject();
        }
    }
    loadData() {
        this.Auth.alluser().subscribe(search => {
            // this.source.load(search.user);
            // console.log (search.data);
            this.source.load(search.user);
        })
    }
    onCreateConfirm2(event) {
        if (window.confirm('Are you sure you want to Create?')) {
            // console.log (event.newData.emp_id);
            const user_id = event.newData.user_id;
            const user_pass = event.newData.user_pass;
            this.Auth.AddManager(user_id, user_pass).subscribe(data => {
                    // this.source.load(search.user);
                    // console.log (search.data);
                    // this.source.load(search.data);
            })
            event.confirm.resolve();
            this.loadData2();
        } else {
            event.confirm.reject();
        }
    }
    loadData2() {
        this.Auth.search_manager().subscribe(search => {
            this.source2.load(search.data);
        })
    }
}

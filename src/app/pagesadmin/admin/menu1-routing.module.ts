import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Menu1Component } from './menu1.component';
import { AlluserComponent } from './alluser/alluser.component';
import { Submenu2Component } from './submenu2/submenu2.component';

const routes: Routes = [{
  path: '',
  component: Menu1Component,
  children: [{
    path: 'alluser',
    component: AlluserComponent,
  }, {
    path: 'submenu2',
    component: Submenu2Component,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class Menu1RoutingModule {

}

export const routedComponents = [
  Menu1Component,
    AlluserComponent,
    Submenu2Component,
];

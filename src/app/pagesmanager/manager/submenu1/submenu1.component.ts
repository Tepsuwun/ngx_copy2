import {Component, OnInit} from '@angular/core';

import { AuthService } from '../../../auth.service';

@Component({
  selector: 'ngx-submenu1',
  styleUrls: ['./submenu1.component.scss'],
  templateUrl: './submenu1.component.html',
})
export class Submenu1Component implements OnInit{
    constructor(private Auth: AuthService ) {
    }
    users = [];
    // hostUrl = "http://localhost/scode_downTime/";
    hostUrl = "http://scodedev.com/laravel/";
    // user_status = [];
    starRate = 2;
    starRate2 = 4;
    starRate3 = 5;
    starRate4 = 3;
    ngOnInit() {
        this.Auth.all_register().subscribe(data => {
            this.users = data.users;
            console.log (this.users);
        })
    }
}

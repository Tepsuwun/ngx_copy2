import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login2/login.component';
import { AuthGuard } from './auth.guard';
import { ManagerGuard } from './manager.guard';
import { AdminGuard } from './admin.guard';
import { LoginGuard } from './login.guard';

const routes: Routes = [
  { path: 'pages', loadChildren: 'app/pages/pages.module#PagesModule' , canActivate: [AuthGuard]},
    { path: 'manager', loadChildren: 'app/pagesmanager/pages.module#PagesModule' , canActivate: [ManagerGuard]},
    { path: 'admin', loadChildren: 'app/pagesadmin/pages.module#PagesModule' , canActivate: [AdminGuard]},
    { path: 'login', loadChildren: 'app/pageslogin/pages.module#PagesModule' , canActivate: [LoginGuard]},
    {
        path: 'logout',
        component: LoginComponent,
    },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'login' },
];

const config: ExtraOptions = {
  useHash: true,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}

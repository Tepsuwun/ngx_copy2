import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AdminGuard implements CanActivate {
    constructor(private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      let user_status = localStorage.getItem('user_status');
      if (user_status == '4') {
          console.log ('user_status => ', user_status , '(เข้าสู่ระบบสำเร็จ)');
          return true
      }else {
          console.log ('user_status => ', user_status , '(เข้าสู่ระบบไม่สำเร็จ)');
          // localStorage.clear();
          this.router.navigate(['/login'])
          return false
      }
  }
}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class LoginGuard implements CanActivate {
    constructor(private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      let user_status = localStorage.getItem('user_status');
      switch (user_status) {
          case '1': {
              let emp_data = localStorage.getItem('emp_data');
              if (emp_data != '0') {
                  this.router.navigate(['/pages']);
              }
            break;
          }
          case '2': {
              let emp_data = localStorage.getItem('emp_data');
              if (emp_data != '0') {
                  this.router.navigate(['/pages']);
              }
              // this.router.navigate(['/pages']);
              break;
          }
          case '3': {
              this.router.navigate(['/manager']);
              break;
          }
          case '4': {
              this.router.navigate(['/admin']);
              break;
          }
          default: {
              // this.router.navigate(['']);
              break;
          }
      }
    return true;
  }
}

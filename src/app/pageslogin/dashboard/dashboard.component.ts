import {Component, OnDestroy, OnInit} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { Router } from '@angular/router';
import {AuthService} from "../../auth.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
    alluser = [];
    text = "test";
    loginForm: FormGroup;
    isSubmitted: boolean = false;
    constructor(private Auth: AuthService,
                private router: Router) { }
    ngOnInit() {
        // localStorage.clear();
        this.Auth.search_user().subscribe(data => {
            this.alluser = data.data;
        })
        let emp_data = localStorage.getItem('emp_data');
        if (emp_data == '1') {
            this.router.navigate(['/pages']);
        }

        this.loginForm = new FormGroup({
            username: new FormControl('', [
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(10),
                // Validators.pattern('[a-zA-Z ]*')
            ]),
            password: new FormControl('', [
                Validators.required,
                // Validators.pattern('[a-zA-Z ]*')
            ])
        })

    }

    loginUser(event) {
        this.isSubmitted = true
        if (this.loginForm.valid == true) {
            event.preventDefault()
            const target = event.target
            const username = target.querySelector('#username').value
            const password = target.querySelector('#password').value
            this.Auth.loginUser(username, password).subscribe(data => {
                if (data.success) {
                    switch (data.user_status) {
                        case 1 : {
                            console.log(1);
                            localStorage.setItem('emp_data', data.emp_data);
                            if (data.emp_data == '0') {
                                this.router.navigate(['login/profile']);
                            } else {
                                this.router.navigate(['/pages']);
                            }
                            break;
                        }
                        case 2 : {
                            this.router.navigate(['/pages']);
                            console.log(2);
                            localStorage.setItem('emp_data', data.emp_data);
                            if (data.emp_data == '0') {
                                this.router.navigate(['login/profile']);
                            } else {
                                this.router.navigate(['/pages']);
                            }
                            break;
                        }
                        case 3 : {
                            this.router.navigate(['/manager']);
                            console.log(3);
                            break;
                        }
                        case 4 : {
                            console.log(4);
                            this.router.navigate(['/admin']);
                            break;
                        }
                        default : {
                            this.router.navigate(['/login']);
                            console.log(5);
                            break;
                        }
                    }
                    localStorage.setItem('username', username);
                    localStorage.setItem('password', password);
                    localStorage.setItem('emp_id', data.emp_id);
                    /*this.Auth.getEmployee(username, password).subscribe(data2 => {
                        if (data2.emp_data == '0') {
                            this.router.navigate(['pages/employee/one'])
                        }else{
                            this.router.navigate([''])
                        }
                        localStorage.setItem('emp_data', data2.emp_data);
                    })*/
                    // this.router.navigate([''])
                    this.Auth.setLoggedIn(true)
                } else {
                    localStorage.setItem('username', username);
                    localStorage.setItem('password', password);
                    localStorage.setItem('emp_id', data.emp_id);
                    // window.alert(data.message)
                }
                localStorage.setItem('user_status', data.user_status.toString());
                localStorage.setItem('loginSuccess', data.success);
                let myItem = localStorage.getItem('loginSuccess');
                console.log('loginSuccess => ', myItem);
            })
            console.log(username, password)
        }
    }

}

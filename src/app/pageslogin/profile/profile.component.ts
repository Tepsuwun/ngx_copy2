import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "../../auth.service";
import { HttpClient } from '@angular/common/http';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    input = {};
    inputs = [];
    firstSubmitted: boolean = false;
    isSubmitted: boolean = false;
    isImages: boolean = false;
    profileForm: FormGroup;

    constructor(private Auth: AuthService,
                private router: Router,
                private http: HttpClient,
                private formBuilder: FormBuilder) { }
    ngOnInit() {
        this.profileForm = new FormGroup({
            FirstName: new FormControl('', [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(30),
                // Validators.pattern('[a-zA-Z ]*')
            ]),
            LastName: new FormControl('', [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(30)
            ]),
            NickName: new FormControl('', [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(20)
            ]),
            Birthday: new FormControl('', [
                Validators.required
            ]),
            Weight: new FormControl('', [
                Validators.required,
                Validators.min(10),
                Validators.max(300)
            ]),
            Height: new FormControl('', [
                Validators.required,
                Validators.min(20),
                Validators.max(400)
            ]),
            Address: new FormControl('', [
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(1000)
            ]),
        })
    }
    radioSelected: string;
    selectedFile: File = null;
    inputSex: string = "Female";
    OnSubmit(event) {
        this.isSubmitted = true;
        console.log ("isImages" , this.isImages)
        console.log ("profileForm" ,this.profileForm.valid);
        // console.log (this.profileForm.controls.inputFirstName.valid);
        if (this.profileForm.valid == true) {
            this.inputs = [];
            event.preventDefault()
            // const inputSex = this.inputSex
            const target = event.target
            const inputFirstName = target.querySelector('#inputFirstName').value
            const inputLastName = target.querySelector('#inputLastName').value
            const inputNickName = target.querySelector('#inputNickName').value
            const inputBirthday = target.querySelector('#inputBirthday').value
            const inputAddress = target.querySelector('#inputAddress').value
            const inputWeight = target.querySelector('#inputWeight').value
            const inputHeight = target.querySelector('#inputHeight').value
            this.inputs.push({
                inputSex: this.inputSex,
                inputFirstName: inputFirstName,
                inputLastName: inputLastName,
                inputNickName: inputNickName,
                inputBirthday: inputBirthday,
                inputAddress: inputAddress,
                inputWeight: inputWeight,
                inputHeight: inputHeight,
                emp_id: localStorage.getItem('emp_id'),
                emp_data: '1',
                // inputFile: this.imageUrl,
            })
            if (this.imageUrl != "/assets/images/default-image.png"){
                this.inputs.push({
                    // inputFile: this.imageUrl,
                    inputFile: this.imageUrl,
                })
            }else {
                this.inputs.push({
                    inputFile: null,
                })
            }
            const emp_id = localStorage.getItem('emp_id');
            const formData: FormData = new FormData();
            formData.append('Image', this.fileToUpload, this.fileToUpload.name);
            formData.append('emp_id', emp_id);
            this.Auth.UploadImg(formData).subscribe(data => { });

            this.Auth.UpdateRegister(this.inputs).subscribe(data => {
                console.log ("UpdateRegister" , data.status)
                if (data.status == '200') {
                    localStorage.setItem('emp_data', '1');
                    this.router.navigate(['/page'])
                }
            })
        }else {
            if (this.imageUrl != "/assets/images/default-image.png"){
                this.isImages = true
            }else {
                this.isImages = false
            }
        }
    }

    imageUrl: string = "/assets/images/default-image.png";
    fileToUpload: File = null;
    handleFileInput(file: FileList) {
        this.fileToUpload = file.item(0);
        this.isImages = true

        //Show image preview
        var reader = new FileReader();
        reader.onload = (event: any) => {
            this.imageUrl = event.target.result;
            console.log ("imageUrl", this.imageUrl);
        }
        reader.readAsDataURL(this.fileToUpload);
        console.log ("fileToUpload", this.fileToUpload);
    }

    onItemChange(item) {
        this.inputSex = item
        console.log (item);
    }

//    *************************************************************
    handleFileInput2(file: FileList) {
        this.fileToUpload = file.item(0);

        //Show image preview
        var reader = new FileReader();
        reader.onload = (event:any) => {
            this.imageUrl = event.target.result;
        }
        reader.readAsDataURL(this.fileToUpload);
    }

    OnSubmit2(Caption,Image){
        const endpoint = 'http://127.0.0.1:8000/api/testUpload';
        const formData: FormData = new FormData();
        formData.append('Image', this.fileToUpload, this.fileToUpload.name);
        formData.append('ImageCaption', Caption);
        this.http.post(endpoint, formData).subscribe(data => {

        });
    }

}

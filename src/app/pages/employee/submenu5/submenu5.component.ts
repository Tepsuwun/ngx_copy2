import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'ngx-submenu5',
  styleUrls: ['./submenu5.component.scss'],
  templateUrl: './submenu5.component.html',
})
export class Submenu5Component {
    starRate = 0;
    input = {};
    inputs = [];
    firstSubmitted: Boolean = false;

    firstSubmit(firstForm: NgForm) {
        this.firstSubmitted = true;
        if (firstForm.valid) {
            this.inputs.push({
                inputFirstName : firstForm.controls.inputFirstName.value,
                inputLastName : firstForm.controls.inputLastName.value,
                inputNickName : firstForm.controls.inputNickName.value,
                inputPhoneNumber : firstForm.controls.inputPhoneNumber.value,
                inputAddress : firstForm.controls.inputAddress.value,
                inputWeight : firstForm.controls.inputWeight.value,
                inputHeight : firstForm.controls.inputHeight.value,
                // inputImg : firstForm.controls.inputImg.value
            });
            localStorage.setItem('inputs', JSON.stringify(this.inputs));
            this.firstSubmitted = false;
            firstForm.resetForm();
            console.log (this.inputs);
        }
    }

}

import { Component } from '@angular/core';

@Component({
  selector: 'ngx-employee-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class EmployeeComponent {
}

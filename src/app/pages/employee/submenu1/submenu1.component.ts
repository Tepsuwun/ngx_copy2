import {Component, OnInit} from '@angular/core';
import { AuthService } from '../../../auth.service';

@Component({
  selector: 'ngx-submenu1',
  styleUrls: ['./submenu1.component.scss'],
  templateUrl: './submenu1.component.html',
})
export class Submenu1Component implements OnInit {

    constructor(private Auth: AuthService ) {}
    users = []
    user = {}
    ngOnInit() {
        let emp_id = localStorage.getItem('emp_id');
        this.Auth.search_oneuser(emp_id).subscribe(data => {
            this.user = data.data;
        })
    }

}
